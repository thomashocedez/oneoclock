#include <EEPROM.h>
#include "RTClib.h"
#include <SD.h>
#include <Adafruit_GFX.h>    // Core graphics library
#include <Adafruit_ST7735.h> // Hardware-specific library

#define TFT_SCLK 13  
#define TFT_MOSI 11  
#define TFT_CS     10
#define TFT_RST    9  
#define TFT_DC     8
#define SD_CS  4 
Adafruit_ST7735 tft = Adafruit_ST7735(TFT_CS,  TFT_DC, TFT_RST);

// Is the alarm is now ?
bool is_reveil = false;

String inputString = "";         // a string to hold incoming data

unsigned long previousMillis = 0; // Timer for the non-delay actions
const long interval = 1000;           // refresh screen every ....

RTC_DS1307 rtc;

byte t_h = 0;
byte t_m = 0;
byte t_s = 0;
byte t_d = 0;
byte t_n = 0;
int t_y = 2016;
byte a_h = 20;
byte a_m = 7;
bool isUpdating = false;
byte temp = 0;
int tempL = 0, tempH = 0;
String precip, ip = "";
char *forecast = "--";


void setup()
{

  // communication between ESP & Arduino
  Serial.begin(9600);
  // Start RTC
  rtc.begin();
  // Init screen :
  tft.initR(INITR_BLACKTAB);
  tft.setRotation(3);
  tft.fillScreen(ST7735_BLACK); // Clear display
  tft.setCursor(0, 0);
  tft.print("setup begin..\r\n");
  if (!SD.begin(SD_CS)) {
    tft.println("SD failed!");
    return;
  }
  tft.print("SD OK..\r\n");

  // Get back alarms : 
  a_h = EEPROM.read(1);
  a_m = EEPROM.read(2);
  tft.println("Set alarm to : " + String(a_h) + " " + a_m);

  tft.print("Query WU..\r\n");
  delay(3000);

  // Nag screen ;-D
  bmpDraw("loading.bmp", 0, 0);

  delay(3000);
  Serial.print("WU\n");
  // Cleanup ...
  tft.fillScreen(ST7735_BLACK);

}



void loop()
{
  // If no data is incoming : 
  if (!isUpdating)
  {
    unsigned long currentMillis = millis();
    // And if delay is over :
    if (currentMillis - previousMillis >= interval) {
      previousMillis = currentMillis;
      // Update screen
      affiche();
      // TODO : check alarm
      reveil();
    }
  }


}


void reveil() {
  DateTime now = rtc.now();
  volatile int last_m = 0;
  if ( a_h = now.hour() && a_m == now.minute()) {
    is_reveil = true;
  }
}

void serialEvent() {
  while (Serial.available()) {
    // Lock the system :
    isUpdating = true;
    // Getting data :
    char inChar = (char)Serial.read();
    inputString += inChar;
    if (inChar == '\n') {
      String msg = inputString.substring(0, 2);
      // First 2 chars received are commands: 
      if (msg == "Up") {
        tft.setCursor(0, 0);
        tft.setTextSize(0);
        tft.fillScreen(ST7735_BLACK);
      }
      if (msg == "D ") {
        // Day 
        t_d = inputString.substring(2).toInt();

      }
      if (msg == "AH") {
        // Alarm hour
        a_h = inputString.substring(2).toInt();

      } if (msg == "AM") {
        // Alarm minute
        a_m = inputString.substring(2).toInt();

      }
      if (msg == "N ") 
        { //Current monthh
        t_n = inputString.substring(2).toInt();

      }
      if (msg == "Y ") {
        //Current  year
        t_y = inputString.substring(2).toInt();
      }
      if (msg == "H ") {
        // Current hour
        t_h = inputString.substring(2).toInt();

      }
      if (msg == "M ") 
      {
      //Current minute
        t_m = inputString.substring(2).toInt();
      }
      if (msg == "S ") {
        //Current seconds
        t_s = inputString.substring(2).toInt();
      }
      if (msg == "W ") {
        // Message title : not udes
        //msgw =String( inputString.substring(2,12));
      }

      if (msg == "F ") {
        // Current temp
        temp = inputString.substring(2).toInt();
      }
      if (msg == "TH") {
        // Highest temp
        tempH = inputString.substring(2).toInt();
      }
      if (msg == "TL") {
        //Lowest temp
        tempL = inputString.substring(2).toInt();
      }
      if (msg == "P ") {
        // Rain
        precip = inputString.substring(2);
      }
      if (msg == "IP") {
        //Local IP
        ip = inputString.substring(2, 15);
      }
      if (msg == "C ") {
        // Weather condition
        forecast = inputString[2];

      }
      if (msg == "#A") {
        // End of alarmmessage
        EEPROM.write(1, a_h);
        EEPROM.write(2, a_m);
        affiche();
        isUpdating = false;
      }
      if (msg == "#C") {
        // End of conditions messages
        rtc.adjust(DateTime(t_y, t_n, t_d, t_h, t_m, t_s));
        affiche_meteo();
        affiche();
        isUpdating = false;
      }
      inputString = "";

    }
  }
}


void affiche_meteo() {

  tft.fillScreen(ST7735_BLACK);
  char* ff = getCond(forecast);
  tft.setTextColor(ST7735_WHITE, ST7735_BLACK);

  tft.setTextSize(1);
  tft.setCursor(63, 50);
  tft.setTextColor(ST7735_YELLOW);
  tft.print("Meteo");

  tft.setTextColor(ST7735_WHITE);
  tft.setCursor(63, 70);
  tft.print("T ");
  tft.print(String(tempL) + " -> " + String(tempH));
  tft.print("'C");

  tft.setCursor(63, 80);
  tft.print(ff);
  tft.setCursor(63, 90);
  tft.print("Pluie: " + String(precip));

}



void affiche() {
  DateTime now = rtc.now();
  tft.setTextSize(0);
  tft.setCursor(0, 0);

  tft.setTextColor(ST7735_WHITE, ST7735_BLACK);
  tft.setTextSize(1);
  tft.setCursor(10, 10);
  tft.setTextColor(ST7735_YELLOW);

  tft.print(now.day());
  printMonth(now.month()); // month
  tft.print(now.year()); // year
  ///////////////////////// ALARM //////////////////////
  tft.setCursor(100, 10);
  tft.setTextColor(ST7735_RED, ST7735_BLACK);
  tft.print("  " + String(a_h) + ":" + a_m);
  tft.setTextColor(ST7735_WHITE, ST7735_BLACK);
  ///////////////////////// TIME //////////////////////
  tft.setCursor(10, 25);
  tft.setTextSize(3);
  tft.print(now.hour());
  tft.print(":");
  if (now.minute() < 10)
  {
    tft.print("0");
  }
  tft.print(now.minute());
  tft.print(":");

  if (now.second() < 10)
  {
    tft.print("0");
  }
  tft.print(now.second());
}

// Convert code to weather human readable & icon
char* getCond(char* condi)
{
  char* wout = "-----";
  if (condi == 'F') {
    bmpDraw("F.bmp", 0, 60);

    wout = "chanceflurries";
  }
  if (condi == 'Q' ) {
    bmpDraw("Q.bmp", 0, 60);
    wout = "Risque de Pluie";
  }
  if (condi == 'W' ) {
    bmpDraw("W.bmp", 0, 60);

    wout = "Givre"; //OK
  }
  if (condi == 'V' ) {
    bmpDraw("V.bmp", 0, 60);

    wout = "Risque de neige "; //OK
  }
  if (condi == 'S' ) {
    bmpDraw("S.bmp", 0, 60);

    wout = "Risque d'orage"; //OK
  }
  if (condi == 'B' ) {
    bmpDraw("B.bmp", 0, 60);

    wout = "Temp clair"; //OK
  }
  if (condi == 'Y' ) {
    bmpDraw("Y.bmp", 0, 60);

    wout = "Nuageux"; //OK
  }
  if (condi == 'M' ) {
    bmpDraw("M.bmp", 0, 60);

    wout = "Brouillard"; //OK
  }
  if (condi == 'E' ) {
    bmpDraw("E.bmp", 0, 60);

    wout = "hazy";
  }
  if (condi == 'H' ) {
    bmpDraw("H.bmp", 0, 60);

    wout = "Peu nuageux"; //OK
  }
  if (condi == 'J' ) {
    bmpDraw("J.bmp", 0, 60);

    wout = "Ensoleille"; //OK
  }
  if (condi == 'R' ) {
    wout = "Pluie"; //ok
    bmpDraw("R.bmp", 0, 60);

  }
  if (condi == '0' ) {
    bmpDraw("na.bmp", 0, 60);

    wout = "----";
  }
  return wout;
}

// Convert month to month ;-D
void printMonth(int month)
{
  switch (month)
  {
    case 1: tft.print(" Janvier "); break;
    case 2: tft.print(" Fevrier "); break;
    case 3: tft.print(" Mars "); break;
    case 4: tft.print(" Avril "); break;
    case 5: tft.print(" Mai "); break;
    case 6: tft.print(" Juin "); break;
    case 7: tft.print(" Juillet "); break;
    case 8: tft.print(" Aout "); break;
    case 9: tft.print(" Septembre "); break;
    case 10: tft.print(" Octobre "); break;
    case 11: tft.print(" Novembre "); break;
    case 12: tft.print(" Decembre "); break;
    default: tft.print(" Error "); break;
  }
}

// gfx routines

#define BUFFPIXEL 10

void bmpDraw(char *filename, uint8_t x, uint8_t y) {

  File     bmpFile;
  int      bmpWidth, bmpHeight;   // W+H in pixels
  uint8_t  bmpDepth;              // Bit depth (currently must be 24)
  uint32_t bmpImageoffset;        // Start of image data in file
  uint32_t rowSize;               // Not always = bmpWidth; may have padding
  uint8_t  sdbuffer[3 * BUFFPIXEL]; // pixel buffer (R+G+B per pixel)
  uint8_t  buffidx = sizeof(sdbuffer); // Current position in sdbuffer
  boolean  goodBmp = false;       // Set to true on valid header parse
  boolean  flip    = true;        // BMP is stored bottom-to-top
  int      w, h, row, col;
  uint8_t  r, g, b;
  uint32_t pos = 0, startTime = millis();

  if ((x >= tft.width()) || (y >= tft.height())) return;


  // Open requested file on SD card
  if ((bmpFile = SD.open(filename)) == NULL) {
    tft.println("File not found");
    return;
  }

  // Parse BMP header
  if (read16(bmpFile) == 0x4D42) { // BMP signature
    read32(bmpFile);
    (void)read32(bmpFile); // Read & ignore creator bytes
    bmpImageoffset = read32(bmpFile); // Start of image data

    // Read DIB header
    read32(bmpFile);
    bmpWidth  = read32(bmpFile);
    bmpHeight = read32(bmpFile);
    if (read16(bmpFile) == 1) { // # planes -- must be '1'
      bmpDepth = read16(bmpFile); // bits per pixel
      if ((bmpDepth == 24) && (read32(bmpFile) == 0)) { // 0 = uncompressed

        goodBmp = true; // Supported BMP format -- proceed!

        // BMP rows are padded (if needed) to 4-byte boundary
        rowSize = (bmpWidth * 3 + 3) & ~3;

        // If bmpHeight is negative, image is in top-down order.
        // This is not canon but has been observed in the wild.
        if (bmpHeight < 0) {
          bmpHeight = -bmpHeight;
          flip      = false;
        }

        // Crop area to be loaded
        w = bmpWidth;
        h = bmpHeight;
        if ((x + w - 1) >= tft.width())  w = tft.width()  - x;
        if ((y + h - 1) >= tft.height()) h = tft.height() - y;

        // Set TFT address window to clipped image bounds
        tft.setAddrWindow(x, y, x + w - 1, y + h - 1);

        for (row = 0; row < h; row++) { // For each scanline...

          // Seek to start of scan line.  It might seem labor-
          // intensive to be doing this on every line, but this
          // method covers a lot of gritty details like cropping
          // and scanline padding.  Also, the seek only takes
          // place if the file position actually needs to change
          // (avoids a lot of cluster math in SD library).
          if (flip) // Bitmap is stored bottom-to-top order (normal BMP)
            pos = bmpImageoffset + (bmpHeight - 1 - row) * rowSize;
          else     // Bitmap is stored top-to-bottom
            pos = bmpImageoffset + row * rowSize;
          if (bmpFile.position() != pos) { // Need seek?
            bmpFile.seek(pos);
            buffidx = sizeof(sdbuffer); // Force buffer reload
          }

          for (col = 0; col < w; col++) { // For each pixel...
            // Time to read more pixel data?
            if (buffidx >= sizeof(sdbuffer)) { // Indeed
              bmpFile.read(sdbuffer, sizeof(sdbuffer));
              buffidx = 0; // Set index to beginning
            }

            // Convert pixel from BMP to TFT format, push to display
            b = sdbuffer[buffidx++];
            g = sdbuffer[buffidx++];
            r = sdbuffer[buffidx++];
            tft.pushColor(tft.Color565(r, g, b));
          } // end pixel
        } // end scanline
      } // end goodBmp
    }
  }

  bmpFile.close();
}

// These read 16- and 32-bit types from the SD card file.
// BMP data is stored little-endian, Arduino is little-endian too.
// May need to reverse subscript order if porting elsewhere.

uint16_t read16(File f) {
  uint16_t result;
  ((uint8_t *)&result)[0] = f.read(); // LSB
  ((uint8_t *)&result)[1] = f.read(); // MSB
  return result;
}

uint32_t read32(File f) {
  uint32_t result;
  ((uint8_t *)&result)[0] = f.read(); // LSB
  ((uint8_t *)&result)[1] = f.read();
  ((uint8_t *)&result)[2] = f.read();
  ((uint8_t *)&result)[3] = f.read(); // MSB
  return result;
}
