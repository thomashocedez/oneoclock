/**The MIT License (MIT)

  Copyright (c) 2016 by Daniel Eichhorn

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  See more at http://blog.squix.ch
*/
#include <WiFiManager.h>
#include <ESP8266WiFi.h>
#include <Ticker.h>
#include "settings.h"
#include <JsonListener.h>
#include <ArduinoOTA.h>
#include <ESP8266mDNS.h>
#include <time.h>
#include <DNSServer.h>
#include <ESP8266WebServer.h>
#include <WiFiManager.h>

#include "WundergroundClient.h"

ESP8266WebServer server(80);


String inputString = "";         // a string to hold incoming data




// Set to false, if you prefere imperial/inches, Fahrenheit
WundergroundClient wunderground(IS_METRIC);


// flag changed in the ticker function every 10 minutes
bool readyForWeatherUpdate = true;

String lastUpdate = "--";

Ticker ticker;

void setReadyForWeatherUpdate();




void setup() {
  Serial.begin(9600);
  Serial.println();
  Serial.println();
  WiFiManager wifiManager;
  
  // Uncomment for testing wifi manager
  // wifiManager.resetSettings();
  wifiManager.setAPCallback(configModeCallback);

  //or use this for auto generated name ESP + ChipID
  wifiManager.autoConnect();
  String hostname(HOSTNAME);
  hostname += String(ESP.getChipId(), HEX);
  WiFi.hostname(hostname);

  int counter = 0;
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.println("# Connecting");
    counter++;
  }
  Serial.println("# Connected");
  ticker.attach(UPDATE_INTERVAL_SECS, setReadyForWeatherUpdate);


  if (MDNS.begin("oneoclock")) {
    //Serial.println("MDNS responder started");
  }

  server.on("/", handleRoot);

  server.on("/set", save_alarm);

  server.onNotFound(handleNotFound);

  server.begin();
  //Serial.println("HTTP server started");
}

void loop() {
  if (readyForWeatherUpdate ) {
    updateData();
  }
  server.handleClient();
  while (Serial.available()) {
    char inChar = (char)Serial.read();
    inputString += inChar;
    if (inChar == '\n') {
      if (inputString.substring(0, 2) == "WU") {
        readyForWeatherUpdate = true;
      }
      inputString = "";
    }
  }

}

void configModeCallback (WiFiManager *myWiFiManager) {
  Serial.println("Entered config mode");
  Serial.println(WiFi.softAPIP());
  //if you used auto generated SSID, print it
  Serial.println(myWiFiManager->getConfigPortalSSID());
  tft.drawString(64, 10, "Wifi Manager");
  display.drawString(64, 20, "Please connect to AP");
  display.drawString(64, 30, myWiFiManager->getConfigPortalSSID());
  display.drawString(64, 40, "To setup Wifi Configuration");
  display.display();*/
}
void updateData() {
 Serial.println( "# Updating conditions...");
  wunderground.updateConditions(WUNDERGRROUND_API_KEY, WUNDERGRROUND_LANGUAGE, WUNDERGROUND_COUNTRY, WUNDERGROUND_CITY);
  Serial.println( "# Updating forecasts...");
  wunderground.updateForecast(WUNDERGRROUND_API_KEY, WUNDERGRROUND_LANGUAGE, WUNDERGROUND_COUNTRY, WUNDERGROUND_CITY);
  Serial.print("W ");
  Serial.println(wunderground.getWeatherText());
  Serial.print("F ");
  Serial.println( wunderground.getCurrentTemp());
  Serial.print("C ");
  Serial.println( wunderground.getForecastIcon(0));
  Serial.print("P ");
  Serial.println( wunderground.getPrecipitationToday());
  Serial.print("TL");
  Serial.println( wunderground.getForecastLowTemp(0));
  Serial.print("TH");
  Serial.println( wunderground.getForecastHighTemp(0));
  Serial.println( "# Updating time...");
  Serial.println("#C Done...");
  Serial.print("H ");
  Serial.println();
  Serial.print("M ");
  Serial.println();
  Serial.print("S ");
  Serial.println();
  readyForWeatherUpdate = false;

}
void setReadyForWeatherUpdate() {
  readyForWeatherUpdate = true;
}

void save_alarm() {
  if (server.hasArg("ah") && server.hasArg("am")) {
    Serial.print("AH");
    Serial.println(server.arg("ah"));
    Serial.print("AM");
    Serial.println(server.arg("am"));
    Serial.println("#A Done...");
    String header = "HTTP/1.1 301 OK\r\nSet-Cookie: ESPSESSIONID=1\r\nLocation: /\r\nCache-Control: no-cache\r\n\r\n";
    server.sendContent(header);
  }
}
void handleRoot() {
  String content = "<html><body><form action='/set' method='POST'>Réveil :<br>";
  content += "Heure :<input type='text' name='ah' placeholder='07'><br>";
  content += "Minute:<input type='text' name='am' placeholder='30'><br>";
  content += "<input type='submit' name='SUBMIT' value='Valider'></form><br>";
  server.send(200, "text/html", content);
}


void handleNotFound() {
  String message = "File Not Found\n\n";
  message += "URI: ";
  message += server.uri();
  message += "\nMethod: ";
  message += (server.method() == HTTP_GET) ? "GET" : "POST";
  message += "\nArguments: ";
  message += server.args();
  message += "\n";
  for (uint8_t i = 0; i < server.args(); i++) {
    message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
  }
  server.send(404, "text/plain", message);
}
